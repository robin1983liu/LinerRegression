import pandas as pd
import numpy as np

# 设置随机种子以保证结果可重复
np.random.seed(42)

# 生成1000个随机房间数量 (RM)
rm = np.random.normal(loc=6, scale=1, size=1000)

# 生成1000个房价中位数 (MEDV)，与房间数量相关，并加入随机噪声
medv = 20 + 5 * rm + np.random.normal(loc=0, scale=5, size=1000)

# 创建DataFrame
data = pd.DataFrame({'RM': rm, 'MEDV': medv})

# 保存为CSV文件
data.to_csv('housing.csv', index=False)